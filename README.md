# TEST ARCA

Debe recibir como parámetro un arreglo de cadena caracteres que representan cada fila de una tabla de **NxN** con la secuencia del ADN. Las letras de los caracteres solo pueden ser: **A, T, C, G**; las cuales representan cada base nitrogenada del ADN.

Sabrás si existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua, horizontal o vertical.

# Instalación

Clonar el repositorio

    git clone https://gitlab.com/edgar_dorantes/arca-test.git
    
Entrar al proyecto y instalar las dependencias, debes tener Nodejs si no lo tienes sigue las instrucciones del siguiente link [https://nodejs.org/es/download/package-manager/](https://nodejs.org/es/download/package-manager/)

    npm install

Ejecutar el siguiente comando 

    node .

# Ver prueba en local

***Recurso API:*** http://localhost:3000

# Ver prueba en linea

***Recurso API:*** http://ec2-18-223-109-186.us-east-2.compute.amazonaws.com

# Recusros

## Verificar mutación

**POST** /mutation

Ejemplo de body sin mutación.

	
    {"dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]} 

> Request status 403

    
 

Ejemplo de body con mutación.

    {"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]}

> Request status 200

## Ver estadísticas de la mutación

**GET** /mutation/stats


Ejemplo de resultado 

    
    {"count_mutations": 40,"count_no_mutation": 100,"ratio": 0.4}