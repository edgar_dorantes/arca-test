'use strict';

module.exports = function(Mutation) {

    Mutation.regex = function (list) {
        const regex = /(([A]{4,})|([T]{4})|([C]{4})|([G]{4})\w*)/;
        return regex.test(list);
    }

    Mutation._oblicuo = function (dna) {
        let oblicuo = [];
        dna.forEach((_adn, i) => {
            let contList = 0;
            _adn.split('').forEach((latter,_i) => {
                if (i === 0) {
                    oblicuo[_i] = latter;
                } else if (_i >= i) {
                    oblicuo[contList] = oblicuo[contList] + latter;
                    contList ++;
                }
            });
        });
        return oblicuo;
    } 

    Mutation._vertical = function (dna) {
        let list = [];
        dna.forEach((_adn, i) => {
            let contList = 0;
            _adn.split('').forEach((latter,_i) => {
                list[_i] = i ? list[_i]+latter : latter
            });
        });
        return list;
    }

    Mutation.validate = function (dna) {
        let mutation = false;
        let listh = dna;
        let listv = Mutation._vertical(dna);
        let oblicuo = [].concat(
            Mutation._oblicuo(dna),
            Mutation._oblicuo(
                dna.map(latter => latter.split('').reverse().join(''))
            ),
            Mutation._oblicuo(listv),
            Mutation._oblicuo(
                listv.map(latter => latter.split('').reverse().join(''))
            )
        );
        let index = 0;
        dna = listh.concat(listv, oblicuo);
        while (index < dna.length) {
            if (Mutation.regex(dna[index])) {
                mutation = true;
                break;
            }
            index ++;
        }
        return mutation;
    }

    Mutation.stats = async function () {
        try {
            const totalNoMutation = await Mutation.count({
                isMutation: false
            });
            const totalMutation = await Mutation.count({
                    isMutation: true
            });
            const ratio = (totalMutation/totalNoMutation);
            return {
                count_mutations: totalMutation,
                count_no_mutations: totalNoMutation,
                ratio: ratio ? ratio : 1
            }
        } catch (e) {
            throw e;
        }
    }


    Mutation.observe('before save', function(ctx, next) {
        const dna = ctx.instance.dna;
        const regex = /(^[ATCG]+$)/g;
        const isValid = regex.test(dna.join(''));
        if (!isValid) {
            var err = new Error("Los caracteres aceptados son A,T,C,G");
            err.statusCode = 400;
            return next(err);
        }
        ctx.instance.isMutation = Mutation.validate(dna);
        next();
    });

    Mutation.observe('after save', function(ctx, next) {
        if (!ctx.instance.isMutation) {
            var err = new Error("No contiene mutación");
            err.statusCode = 403;
            return next(err);
        }
        next(null,{})
    });
      



};
